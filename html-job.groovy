def report_directory = 'reports/*.xml'

def reportResult(directory) {
  junit directory
}

node {
  stage("Clone") {
    git "https://gitlab.com/tixinepi/testsuite.git"
  }

  stage("Test") {
    try {
      sh "behave --junit --junit-directory reports"
    } catch(err) {
      reportResult(report_directory)
      error "${err}"
    }
  }

  stage("Report") {
    reportResult(report_directory)
  }
}