def reportFileLocation = 'report.html'

def reportResult(reportFile) {
  publishHTML([
    allowMissing: false,
    alwaysLinkToLastBuild: false,
    keepAll: false,
    reportDir: '',
    reportFiles: reportFile,
    reportName: 'SSH Report'
  ])
}

node {
  stage("Clone") {
    git "https://gitlab.com/tixinepi/testsuite.git"
  }

  stage("Test") {
    withEnv(['SSH_PW=infralovers', 'SSH_USER=commandemy', 'SSH_HOST=localhost']) {
      try {
        sh "pytest tests/ssh_test.py --html=${reportFileLocation}"
      } catch(err) {
        reportResult(reportFileLocation)
        error "${err}"
      }
    }
  }

  stage("Report") {
    reportResult(reportFileLocation)
  }
}