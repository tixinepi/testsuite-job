def reportFileLocation = 'report.html'

def reportResult(reportFile) {
  publishHTML([
    allowMissing: false,
    alwaysLinkToLastBuild: false,
    keepAll: false,
    reportDir: '',
    reportFiles: reportFile,
    reportName: 'API Report'
  ])
}

node {
  stage("Clone") {
    git "https://gitlab.com/tixinepi/testsuite.git"
  }

  stage("Test") {
    withEnv(['GITLAB_TOKEN=22KDA6Zb5rr-z_id6UsX', 'GITLAB_USER=tixinepi', 'GITLAB_PW=tixinepi']) {
      try {
        sh "pytest tests/api_test.py --html=${reportFileLocation}"
      } catch(err) {
        reportResult(reportFileLocation)
        error "${err}"
      }
    }
  }

  stage("Report") {
    reportResult(reportFileLocation)
  }
}